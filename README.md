## NUMBER_PRINTER


#### Short desription
A simple python script that prints numbers from 1 to 10 in your command line.


#### How to run

Make sure you have either python2 or python3 installed on your Linux or OSX computer. They come pre-installed by it is always good to make sure. Run:

```
dimakri@MeMac:~|⇒  which python
python: aliased to /usr/local/bin/python3
```

Download and unzip [zip archive](https://gitlab.com/dimakri/print_numbers/-/archive/master/print_numbers-master.zip) or do

```
git clone https://gitlab.com/dimakri/print_numbers.git
```

_cd_ to  print_numbers directory


grant execute on _print_numbers.py_:
```
chmod +x print_numbers.py
```

To run do _./print_numbers.py_ or _python print_numbers.py_. The output should be:

```
dimakri@MeMac:~/Desktop|
⇒  ./print_numbers.py
1
2
3
4
5
6
7
8
9
10
```
